/*
 * main.cpp
 *
 *  Created on: May 26, 2014
 *      Author: yuliang
 */

#include <assert.h>
#include "TPBoostTcpService.h"
#include "PLog.h"
#include "PRedisBase.h"
#include "PIOStreamOperator.h"

//#define TEST_TCP_SERVICE	1
#define TEST_REDIS			1

int main() {
	Initlog("config.log", 0);

#ifdef TEST_REDIS
	CPRedisBase	tRedis;
	CPRedisBase::sRedisConfig tConfig;
	tConfig.host = "127.0.0.1";
	tConfig.db = "14";
	tConfig.port = 6379;
	tConfig.timeout = 3;
	if (!tRedis.TimeConnect(&tConfig)) {
		assert(false);
	}

	std::string tValue;

	tRedis.StringAdd("1", "1");
	tRedis.StringGet("1", tValue);
	if (tValue.compare("1") != 0) {
		assert(false);
	}

	tValue.clear();
	tRedis.StringIncrby("1", 2);
	tRedis.StringGet("1", tValue);
	if (tValue.compare("3") != 0) {
		assert(false);
	}

	tValue.clear();
	tRedis.DelKey("1");
	if (0 != tRedis.StringGet("1", tValue)) {
		assert(false);
	}

	tValue.clear();
	tRedis.HashAdd("2", "1", "1");
	tRedis.HashGet("2", "1", tValue);
	if (tValue.compare("1") != 0) {
		assert(false);
	}

	tValue.clear();
	tRedis.HashIncrby("2", "1", 2);
	tRedis.HashGet("2", "1", tValue);
	if (tValue.compare("3") != 0) {
		assert(false);
	}

	tValue.clear();
	tRedis.HashDel("2", "1");
	tRedis.HashGet("2", "1", tValue);
	if (tRedis.HashGet("2", "1", tValue) != 0 || !tValue.empty()) {
		assert(false);
	}

	tValue.clear();
	CPRedisBase::VecStr_t tFields;
	tFields << "2" << "3";
	CPRedisBase::VecStr_t tValues;
	tValues << "2" << "3";
	tRedis.HashMAdd("2", tFields, tValues);

	tValues.clear();
	if (2 != tRedis.HashMGet("2", tFields, tValues)) {
		assert(false);
	}

	tValues.clear();
	tFields << "4";
	if (3 != tRedis.HashMGet("2", tFields, tValues) || tValues[2] != "") {
		assert(false);
	}
	if (2 != tRedis.HashMDel("2", tFields)) {
		assert(false);
	}
	if (0 != tRedis.HashDel("2", "1")) {
		assert(false);
	}

	if (1 != tRedis.SetAdd("3", "1")) {
		assert(false);
	}
	if (0 != tRedis.SetAdd("3", "1")) {
		assert(false);
	}
	tValues.clear();
	tValues << "1" << "2" << "3";
	if (2 != tRedis.SetMAdd("3", tValues)) {
		assert(false);
	}
	if (1 != tRedis.SetExist("3", "2")) {
		assert(false);
	}
	if (0 != tRedis.SetExist("3", "300")) {
		assert(false);
	}
	if (1 != tRedis.SetDel("3", "1")) {
		assert(false);
	}
	if (0 != tRedis.SetDel("3", "1")) {
		assert(false);
	}
	tValues.clear();
	tValues << "2" << "3";
	if (2 != tRedis.SetMDel("3", tValues)) {
		assert(false);
	}
	if (0 != tRedis.SetMDel("3", tValues)) {
		assert(false);
	}

#endif

#ifdef TEST_TCP_SERVICE
	boost::asio::io_service ser;
	uint16_t port = 10000;
	uint32_t maxBufSize = 2048;
	CTPBoostTcpService tTestBoostTcpService(ser, port, maxBufSize);
	ser.run();
#endif

	return 0;
}
