/*
 * TPBoostTcpService.cpp
 *
 *  Created on: May 26, 2014
 *      Author: yuliang
 */

#include "TPBoostTcpService.h"

#include "PLog.h"

CTPBoostTcpService::CTPBoostTcpService(io_service& ioservice, uint16_t port, uint32_t maxBufSize) :
	CTcpService(ioservice, port, maxBufSize) {
	// TODO Auto-generated constructor stub


handler=chpktCreate(m_MaxBufSize);
}

CTPBoostTcpService::~CTPBoostTcpService() {
	// TODO Auto-generated destructor stub
	chpktRelease(handler);
}

void CTPBoostTcpService::OnAccept(void *connect) {
	LOG_DEBUG("accept.." << &connect);
}

void CTPBoostTcpService::OnReceive(const char *buf, uint32_t len, void *cli) {
	LOG_DEBUG("OnReceive.." << &cli);
	chpkgPushData(handler ,buf, len);
	while(chpkgHasComplatePacket(handler)){
		//5 处理包
		int sn = chpkgGetCurrentPacketLength(handler);
		LOG_DEBUG("================" << sn);

	   //6 处理完一个包，需调用
	   chpkgCurrentPacketDone(handler);
	 }
}

void CTPBoostTcpService::OnDisconnect(void *cli) {
	LOG_DEBUG("OnDisconnect..");

}

void CTPBoostTcpService::OnWriteError(const char *buf, uint32_t len, void *cli) {
	LOG_DEBUG("OnWriteError..");

}

