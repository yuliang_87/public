/*
 * TPBoostTcpService.h
 *
 *  Created on: May 26, 2014
 *      Author: yuliang
 */

#ifndef TPBOOSTTCPSERVICE_H_
#define TPBOOSTTCPSERVICE_H_

#include "PBoostSocketSerivce.h"

#include "PSplitPacket.h"

class CTPBoostTcpService : public CTcpService {
public:
	CTPBoostTcpService(io_service& ioservice, uint16_t port, uint32_t maxBufSize);
	virtual ~CTPBoostTcpService();

	virtual void OnAccept(void *);
	virtual void OnReceive(const char *, uint32_t, void *);
	virtual void OnDisconnect(void *);
	virtual void OnWriteError(const char *, uint32_t, void *);

	CPDHANDLE handler;
};

#endif /* TPBOOSTTCPSERVICE_H_ */
