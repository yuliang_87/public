/*
 * OWSThreadManager.h
 *
 *  Created on: Oct 17, 2013
 *      Author: yuliang
 */

#ifndef OWSTHREADMANAGER_H_
#define OWSTHREADMANAGER_H_

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <stddef.h>

#include "PThread.h"

/*
 * @brief       线程管理类
 * @auther      lyl
 */
class CPThreadManager {
public:
	static CPThreadManager* Getinstance();
	virtual ~CPThreadManager();

	/*
	 * @brief       添加一个线程
	 * @auther      lyl
	 * @param [in]  R        : 线程执行体类(必须继承CPThreadRunner)
	 * @param [in]  infinite : 是否无限执行
	 * @param [in]  mailname : 接收消息的容器名称
	 * @return      bool     : false表示失败,true表示成功
	 */
	template<typename R = CPThreadRunner>
	bool AddThread(bool infinite = false, const char* mailname = NULL);
protected:
	CPThreadManager();
private:
	static CPThreadManager* m_instance;
};

template<typename R>
bool CPThreadManager::AddThread(bool infinite, const char* mailname) {
	CPThread<R> *run_ = new CPThread<R>(infinite, mailname);
	boost::thread thread_(boost::bind(&CPThread<R>::Run, run_));
	return true;
}

#endif /* OWSTHREADMANAGER_H_ */
