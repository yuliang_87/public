/*
 * PBoostLocalDate.cpp
 *
 *  Created on: Apr 1, 2015
 *      Author: yuliang
 */

#include "PBoostLocalDate.h"

CPBoostLocalDate::CPBoostLocalDate() : m_UpdateDateTimer(nullptr) {
	// TODO Auto-generated constructor stub

}

CPBoostLocalDate::~CPBoostLocalDate() {
	// TODO Auto-generated destructor stub
}

bool CPBoostLocalDate::Init(BoostTimer_t* timer) {
	if (nullptr == timer)
		return true;
	m_UpdateDateTimer = timer;
	timerUpdate();
	return true;
}

