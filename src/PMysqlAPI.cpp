/*
 * PMysqlAPI.cpp
 *
 *  Created on: Sep 9, 2013
 *      Author: yuliang
 */

#include "PMysqlAPI.h"

#define MYSQL_COUNT 1
#define MYSQL_CHARACTER "utf8"

CDBMysql::CDBMysql()
	: m_init(false), m_mysql(NULL), m_port(0) {
	// TODO Auto-generated constructor stub

}

CDBMysql::~CDBMysql() {
	// TODO Auto-generated destructor stub
	Disconnect();
}

bool CDBMysql::Connect(const char* rdbHost, int rdbPort, const char* username,
		const char* password, const char* db) {
	if (m_init)
		return true;
	if (NULL == rdbHost || 0 == rdbPort || NULL == username ||
			NULL == password || NULL == db)
		return false;
	m_mysql = new MYSQL;
	if (NULL == m_mysql)
		return false;

	mysql_init(m_mysql);
	if(!mysql_real_connect(m_mysql, rdbHost, username, password, db, rdbPort, 0, 0)) {
		MLOG_ERROR(LOG_MYSQL, "Connect failed:(" << GetErrMsg(m_mysql) << ") .");
		delete m_mysql;
		m_mysql = NULL;
		return false;
	}
	mysql_set_character_set(m_mysql, MYSQL_CHARACTER);

	m_init = true;
	m_host = std::string(rdbHost);
	m_port = rdbPort;
	m_username = std::string(username);
	m_password = std::string(password);
	m_db = std::string(db);
	return m_init;
}

void CDBMysql::Disconnect() {
	if (m_init) {
		mysql_close(m_mysql);

		delete m_mysql;
		m_mysql = NULL;
		m_init = false;
	}
}

bool CDBMysql::Reconnect(MYSQL* mysql) {
	if (m_init && NULL != mysql) {
		mysql_close(mysql);
		mysql_init(mysql);
		if (mysql_real_connect(mysql, m_host.c_str(), m_username.c_str(), m_password.c_str(), m_db.c_str(), m_port, 0, 0)) {
			mysql_set_character_set(mysql, MYSQL_CHARACTER);
			return true;
		}
	}
	return false;
}

CDBQuery::CDBQuery()
	: m_result(NULL), m_myMysql(NULL), m_DBMysql(NULL) {

}

CDBQuery::~CDBQuery() {
	if (NULL != m_result) {
		mysql_free_result(m_result);
	}
	if (NULL != m_myMysql && NULL != m_DBMysql) {
		m_DBMysql->SetMysql(m_myMysql);
	}
}

bool CDBQuery::ToQuery(CDBMysql* mysql, const char * sql) {
	if (NULL != m_myMysql || NULL != m_DBMysql) {
		MLOG_ERROR(LOG_MYSQL, "CDBQuery can not use twice.");
		return false;
	}
	if (NULL == mysql || !mysql->IsConnect() || NULL == sql) {
		MLOG_ERROR(LOG_MYSQL, "NULL == mysql || !mysql->IsConnect() || NULL == sql");
		return false;
	}

	m_DBMysql = mysql;
	m_myMysql = mysql->GetMysql();

	bool bResult = false;
	int iCount = MYSQL_COUNT;
	do {
		if (mysql_query(m_myMysql, sql)) {
			MLOG_ERROR(LOG_MYSQL, "ToQuery failed:(" << mysql->GetErrMsg(m_myMysql) << ") .");
		} else {
			bResult = true;
			break;
		}
		if (!mysql->Reconnect(m_myMysql)) {
			MLOG_ERROR(LOG_MYSQL, "Reconnect failed:(" << mysql->GetErrMsg(m_myMysql) << ") .");
			break;
		}
	} while (iCount--);

	if (bResult)
		m_result = mysql_use_result(m_myMysql);
	return bResult;
}

