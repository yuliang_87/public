/*
 * PSplitPacket.h
 *
 *  Created on: May 28, 2013
 *      Author: yuliang
 */

#ifndef PSPLITPACKET_H_
#define PSPLITPACKET_H_

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

/**
 * 使用者必须按以下步骤使用本库API:
 //1 创建handler:
 CPDHANDLE handler=chpktCreate(1000);
 ...
 //2 从网络接收数据到raw_buf
 ...
 //3 将raw_buf中的数据加入到hanler中
 chpkgPushData(handler,raw_buf,600);
 //4 查询是否有完整的数据包
 while(chpkgHasComplatePacket(handler)){
   //5 处理包
   int sn=chpkgGetCurrentPacketLength(handler)
   ...
   //6 处理完一个包，需调用
   chpkgCurrentPacketDone(handler);
 }
 //7 可能需要继续从网络接收包，则转到 2
 ...
 //8 完成，收功
 chpktRelease(handler);
 *
 */

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

typedef struct ChessPacketHandler*  CPDHANDLE;//ChessPacketData;

// 整包长度占用字节数(默认为4)
#define PKGSIZEBYTE 4

/**
 * 创建一个包处理对象
 * 跟据参数max_pkg_size分配缓冲
 */
CPDHANDLE chpktCreate(uint32_t max_pkg_size);

/**
 * 删除一个已存在的包处理对象
 */
void chpktRelease(CPDHANDLE handle);

/**
 * 获取最后一次操作本库某个API的错误信息，只有在API返回负数时才需调用此函数
 */
const char* chpkgGetLastErrorMessage(CPDHANDLE handle);

/**
 * 加入新数据，返回0表示成功
 */
int chpkgPushData(CPDHANDLE handle, const char* data, uint32_t size);

/**
 * 查询当前是否有完整的包，返回0表示否，非0表示是
 */
int chpkgHasComplatePacket(CPDHANDLE handle);

/**
 * 表示处理完当前包了,内部会把下一个包置为当前包（如果存在的话)
 */
void chpkgCurrentPacketDone(CPDHANDLE handle);

/**
 * 获取当前包的长度（不带最前面表示长度的4个字节）
 */
uint32_t chpkgGetCurrentPacketLength(CPDHANDLE handle);

/**
 * 获取当前包指针（不带有最前面4字节的长度)
 * 如果要反序列化一个包时，可直接在包缓冲指针上操作
 */
char* chpkgGetCurrentPacketPtr(CPDHANDLE h);

/**
 * 获取当前包和其长度到一个缓冲中(带有最前面的包长度才行。比如要转发一个包时，这就有用了)
 */
void chpkgCopyCurrentPacketAndLength(char* buf_copy_to, CPDHANDLE h);

#ifdef __cplusplus
}
#endif

#endif /* PSPLITPACKET_H_ */
