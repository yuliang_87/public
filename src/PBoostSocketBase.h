/*
 * PBoostSocketBase.h
 *
 *  Created on: May 19, 2014
 *      Author: yuliang
 */

#ifndef PBOOSTSOCKETBASE_H_
#define PBOOSTSOCKETBASE_H_

class CTcpServiceCallbackBase {
public:
	virtual ~CTcpServiceCallbackBase() {}
protected:
	virtual void OnAccept(void *) = 0;
};

class CClientCallbackBase {
public:
	CClientCallbackBase(uint32_t maxBufSize) : m_MaxBufSize(maxBufSize) {}
	virtual ~CClientCallbackBase() {}
	inline uint32_t GetBuffSize() const { return m_MaxBufSize; }
protected:
	virtual void OnReceive(const char *, uint32_t, void *) = 0;
	virtual void OnDisconnect(void *) = 0;
	virtual void OnWriteError(const char *, uint32_t, void *) = 0;
protected:
	const uint32_t m_MaxBufSize;
};

#endif /* PBOOSTSOCKETBASE_H_ */
