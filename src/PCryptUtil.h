/*
 * PCryptUtil.h
 *
 *  Created on: May 13, 2014
 *      Author: yuliang
 */

#ifndef PCRYPTUTIL_H_
#define PCRYPTUTIL_H_

#include <string>

/*!
 * @class CCryptUtil
 * @brief 加密算法工具类
 */
class CCryptUtil {
public:
	enum DesOperation {
		kDesOperationDecrypt = 0,
		kDesOperationEncrypt = 1
	};

	enum DesMethod {
		kDesMethodECB = 0,
		kDesMethodCBC = 1,
	};

	/*!
	 *
	 */
	static std::string UrlEncode(const std::string& str);

	/*!
	 *
	 */
	static std::string UrlDecode(const std::string& str);

	/*!
	 *
	 */
	static std::string Md5(const std::string& str, bool bUpper = false);

	/*!
	 *
	 */
	static std::string Base64Encode(const std::string& str);

	/*!
	 *
	 */
	static std::string Base64Decode(const std::string& str);

	/*!
	 *
	 */
	static std::string Sha1(const std::string& str, bool bUpper = false);

	/*!
	 *
	 */
	static std::string Sha256(const std::string& str, bool bUpper = false);

	/*!
	 * @brief 3des 加密/解密
	 * @param str		input
	 * @param key		密钥 24位, 不足的补0
	 * @param op		kDesOperationDecrypt解密，kDesOperationEncrypt加密
	 * @param method	模式，目前ecb，内部用DES_ecb3_encrypt方法, 明方长度不是8的倍数，补0
	 */
	static std::string TripleDes(const std::string& str, const std::string& key, DesOperation op, DesMethod method =
			kDesMethodECB, bool bUsingBase64 = true);

	/*!
	 * @brief Random [begin, end]
	 */
	static int Random(int begin, int end);
protected:
	CCryptUtil();
	virtual ~CCryptUtil();

private:
	static bool m_bInit;
};

#endif /* PCRYPTUTIL_H_ */
