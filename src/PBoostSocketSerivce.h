/*
 * PBoostSocketSerivce.h
 *
 *  Created on: May 19, 2014
 *      Author: yuliang
 */

#ifndef PBOOSTSOCKETSERIVCE_H_
#define PBOOSTSOCKETSERIVCE_H_

class CBoostSocketSerivce {
public:
	CBoostSocketSerivce();
	virtual ~CBoostSocketSerivce();
};
#include <boost/asio.hpp>

#include <map>
#include <list>
#include <set>

#include "PBoostSocketClient.h"

using namespace boost::asio;
using namespace boost::asio::ip;
using namespace boost::system;

/*
service.h
#include "PBoostSocketSerivce.h"

class CTPBoostTcpService : public CTcpService {
public:
	CTPBoostTcpService(io_service& ioservice, uint16_t port, uint32_t maxBufSize);
	virtual ~CTPBoostTcpService();

	virtual void OnAccept(void *);
	virtual void OnReceive(const char *, uint32_t, void *);
	virtual void OnDisconnect(void *);
	virtual void OnWriteError(const char *, uint32_t, void *);
};

service.cpp
CTPBoostTcpService::CTPBoostTcpService(io_service& ioservice, uint16_t port, uint32_t maxBufSize) :
	CTcpService(ioservice, port, maxBufSize) {
	// TODO Auto-generated constructor stub

}

CTPBoostTcpService::~CTPBoostTcpService() {
	// TODO Auto-generated destructor stub

}

void CTPBoostTcpService::OnAccept(void *) {
	LOG_DEBUG("accept..");
}

void CTPBoostTcpService::OnReceive(const char *buf, uint32_t len, void *cli) {
	LOG_DEBUG("OnReceive..");
}

void CTPBoostTcpService::OnDisconnect(void *cli) {
	LOG_DEBUG("OnDisconnect..");
}

void CTPBoostTcpService::OnWriteError(const char *buf, uint32_t len, void *cli) {
	LOG_DEBUG("OnWriteError..");
}
 */

class CTcpService;
class CTcpAccentClient : public CTcpClient {
private:
	friend class CTcpService;
public:
	CTcpAccentClient(io_service& ioservice, uint32_t maxBufSize, CTcpService *ser) :
		CTcpClient(ioservice, maxBufSize), m_ser(ser) {}
	virtual ~CTcpAccentClient() {}
	inline void Close() { OnDisconnect(this); }
protected:
	virtual void OnReceive(const char *d, uint32_t l, void *c);
	virtual void OnDisconnect(void *c);
	virtual void OnWriteError(const char *d, uint32_t l, void *c);
private:
	CTcpService *m_ser;
};

class CTcpService : protected CTcpServiceCallbackBase, protected CClientCallbackBase {
private:
	friend class CTcpAccentClient;
	typedef std::set<CTcpAccentClient *> set_cli_t;
public:
	CTcpService(io_service& ioservice, uint16_t port, uint32_t maxBufSize);
	virtual ~CTcpService();
	inline void RemoveClient(CTcpAccentClient * acli) { m_SetClients.erase(acli); acli->DeleteClient(); }
private:
	void addservice(uint16_t port);

	void acp_haddle(const error_code &err, tcp::acceptor* acc, CTcpAccentClient *cli);
	inline void receive_haddle(const char *d, uint32_t l, void * c) { OnReceive(d, l, c); }
	inline void disconnect_haddle(void *c) { CTcpAccentClient * acli = static_cast<CTcpAccentClient *>(c); OnDisconnect(c); RemoveClient(acli); }
	inline void writeerr_haddle(const char *d, uint32_t l, void * c) { CTcpAccentClient * acli = static_cast<CTcpAccentClient *>(c); OnWriteError(d, l, c); RemoveClient(acli); }
private:
	io_service& m_IoServ;
	tcp::acceptor *m_AcceptServ;
	set_cli_t m_SetClients;
};

#endif /* PBOOSTSOCKETSERIVCE_H_ */
