/*
 * PLog.cpp
 *
 *  Created on: Sep 6, 2013
 *      Author: yuliang
 */

#include <log4cplus/configurator.h>
#include "PLog.h"

std::string m_fname;
uint32_t m_sec = 0;
boost::thread *m_thread = 0;
void doConfigure();

void Initlog(const std::string &_fname, uint32_t _sec) {
	m_fname = _fname;
	m_sec = _sec;
	PropertyConfigurator::doConfigure(_fname.c_str());
	if (m_sec)
		m_thread = new boost::thread(&doConfigure);
}

void doConfigure() {
	for (;;) {
		boost::this_thread::sleep(boost::posix_time::seconds(m_sec));
		PropertyConfigurator::doConfigure(m_fname.c_str());
	}
}

