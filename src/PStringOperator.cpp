/*
 * PStringOperator.cpp
 *
 *  Created on: Sep 25, 2013
 *      Author: yuliang
 */

#include "PStringOperator.h"

CPStringOperator::CPStringOperator() {
	// TODO Auto-generated constructor stub

}

CPStringOperator::~CPStringOperator() {
	// TODO Auto-generated destructor stub
}

void CPStringOperator::SplitSome(std::vector<std::string> &vs, const std::string &s, const std::string &c) {
	size_t prepos = 0;
	size_t size = s.size();
	size_t findpos = 0;

	for (size_t i = 0; i != size; ++i) {
		std::string sub;
		if (std::string::npos != (findpos = c.find(s.at(i), 0))) {
			if (i != prepos && i)
				sub = s.substr(prepos, i - prepos);
			else
				sub = std::string("");
			prepos = i + 1;
			vs.push_back(sub);
		} else if (i == size - 1) {
			sub = s.substr(prepos, size - prepos);
			vs.push_back(sub);
			break;
		}
	}
	if (std::string::npos != findpos)
		vs.push_back(std::string(""));
}

void CPStringOperator::SplitSome(std::vector<std::string> &vs, const char *s, const char *c) {
	const std::string strs(s);
	const std::string strc(c);
	SplitSome(vs, strs, strc);
}

void CPStringOperator::SplitAll(std::vector<std::string> &vs, const std::string &s, const std::string &c) {
	size_t prepos = 0;
	size_t findpos = 0;
	while (std::string::npos != (findpos = s.find(c, prepos))) {
		vs.push_back(s.substr(prepos, findpos - prepos));
		prepos += (c.size() + findpos - prepos);
		if (findpos == s.size() - c.size())
			vs.push_back(std::string(""));
	}
	if (prepos <= s.size() - 1)
		vs.push_back(s.substr(prepos, s.size() - prepos));
}

void CPStringOperator::SplitAll(std::vector<std::string> &vs, const char *s, const char *c) {
	const std::string strs(s);
	const std::string strc(c);
	SplitAll(vs, strs, strc);
}

