/*
 * PMailBoxs.cpp
 *
 *  Created on: Oct 18, 2013
 *      Author: yuliang
 */

#include "PMailBoxs.h"

boost::mutex CPMailBoxs::m_mailboxsMutex;
map_boxs_t CPMailBoxs::m_mailboxs;

CPMailBoxs::CPMailBoxs(const char* boxname) {
	// TODO Auto-generated constructor stub
	m_mailboxsMutex.lock();
	if (m_mailboxs.find(boxname) != m_mailboxs.end())
		m_box = m_mailboxs[boxname];
	else {
		m_box = new CBoostMailBox<void*>;
		m_mailboxs[boxname] = m_box;
	}
	m_mailboxsMutex.unlock();
}

CPMailBoxs::~CPMailBoxs() {
	// TODO Auto-generated destructor stub
}

bool CPMailBoxs::RemoveBox(const char *boxname) {
	m_mailboxsMutex.lock();
	map_boxs_t::iterator it = m_mailboxs.find(boxname);
	if (it != m_mailboxs.end()) {
		delete it->second;
		m_mailboxs.erase(it);
	}
	m_mailboxsMutex.unlock();
	return true;
}

