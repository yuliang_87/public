/*
 * PDynamicLoadLib.h
 *
 *  Created on: Sep 9, 2013
 *      Author: yuliang
 */

#ifndef PDYNAMICLOADLIB_H_
#define PDYNAMICLOADLIB_H_

#ifdef __cplusplus
extern "C" {
#endif //__cplusplus

#ifdef _WIN32
#include <Windows.h>
#define LHANDLE HMODULE
#else //linux
#include <dlfcn.h>
#define LHANDLE void*
#define LPROCESS void*
#endif //_WIN32

LHANDLE DLL_LoadLibrary(const char * file_name);
int DLL_FreeLibrary(LHANDLE lib);
LPROCESS DLL_GetProcess(LHANDLE lib, const char * proc_name);
const char * DLL_GetError();

#ifdef __cplusplus
}
#endif

#endif /* PDYNAMICLOADLIB_H_ */
