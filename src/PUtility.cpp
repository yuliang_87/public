/*
 * PUtility.cpp
 *
 *  Created on: Oct 11, 2014
 *      Author: yuliang
 */

#include "PUtility.h"

unsigned int CPUtility::m_PUtilitySeed = 0;
char CPUtility::m_PUtilityDate[DateFormatLength] = {0};

CPUtility::CPUtility() {
	// TODO Auto-generated constructor stub

}

CPUtility::~CPUtility() {
	// TODO Auto-generated destructor stub
}

bool CPUtility::GetStringDate(std::string& date, PUtilityDateFlag flag) {
	time_t t = time(nullptr);
	t -= (SECONDS_DATE * flag);
	struct tm *local = nullptr;
	local = localtime(&t);
	if (nullptr == local)
		return false;
	sprintf(m_PUtilityDate, "%4d-%02d-%02d", local->tm_year + 1900, local->tm_mon + 1, local->tm_mday);
	date = m_PUtilityDate;
	return true;
}

bool CPUtility::GetStringTime(std::string& result, int diffTime) {
	time_t t = time(0);
	t += diffTime;
	struct tm *local = nullptr;
	local = localtime(&t);
	if (nullptr == local)
		return false;
	sprintf(m_PUtilityDate, "%4d-%02d-%02d#%02d:%02d:%02d", local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
			local->tm_hour, local->tm_min, local->tm_sec);
	result = m_PUtilityDate;
	return true;
}

void CPUtility::GetCurrentDate(VecDateInfo_t& info) {
	if (!info.empty())
		info.clear();

	time_t t = time(nullptr);
	struct tm *local = nullptr;
	local = localtime(&t);
	if (nullptr == local)
		return;

	info.push_back(local->tm_year + 1900);
	info.push_back(local->tm_mon + 1);
	info.push_back(local->tm_mday);
	info.push_back(local->tm_hour);
	info.push_back(local->tm_min);
	info.push_back(local->tm_sec);
}

