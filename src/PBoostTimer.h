/*
 * PBoostTimer.h
 *
 *  Created on: Jun 21, 2014
 *      Author: yuliang
 */

#ifndef PBOOSTTIMER_H_
#define PBOOSTTIMER_H_

#include "PSingleton.h"
#include <boost/asio.hpp>
#include <stdint.h>
#include <queue>

typedef boost::asio::deadline_timer BoostTimer_t;

class CPBoostTimer : public CSingleton<CPBoostTimer> {
public:
	CPBoostTimer();
	virtual ~CPBoostTimer();
	void Init(boost::asio::io_service* io);
	BoostTimer_t* GetTimer();
	void SetTimer(BoostTimer_t* timer);
private:
	BoostTimer_t* newTimer();
private:
	uint32_t m_TimerCount;
	boost::asio::io_service* m_Io;
	std::queue<BoostTimer_t*> m_DeadTimer;
};

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
typedef boost::posix_time::ptime Bptime_t;
class TimeInterval {
public:
	TimeInterval() {}
	~TimeInterval() {}
	void start() { stime = boost::posix_time::microsec_clock::local_time(); }
	void restart() { stime = boost::posix_time::microsec_clock::local_time(); }
	Bptime_t::time_duration_type end() { etime = boost::posix_time::microsec_clock::local_time(); return etime - stime; }
private:
	Bptime_t stime;
	Bptime_t etime;
};

#define TIME_INTERVAL_INIT \
	TimeInterval __FUNCTIOIN__;
#define TIME_INTERVAL_INIT_AND_START \
	TimeInterval __FUNCTIOIN__; \
	__FUNCTIOIN__.start();
#define TIME_INTERVAL_START \
	__FUNCTIOIN__.start();
#define TIME_INTERVAL_END \
	__FUNCTIOIN__.end()

#endif /* PBOOSTTIMER_H_ */
