/*
 * PBoostLocalDate.h
 *
 *  Created on: Apr 1, 2015
 *      Author: yuliang
 */

#ifndef PBOOSTLOCALDATE_H_
#define PBOOSTLOCALDATE_H_

#include "PBoostTimer.h"
#include "PUtility.h"
#include "PSingleton.h"
#include <boost/bind.hpp>

class CPBoostLocalDate : public CSingleton<CPBoostLocalDate> {
public:
	CPBoostLocalDate();
	virtual ~CPBoostLocalDate();
	bool Init(BoostTimer_t* timer);

	inline const std::string& GetTodayDate() { check(); return m_TodayDate; }
	inline const std::string& GetYesterdayDate() { check(); return m_YesterdayDate; }
	inline const std::string& GetCurrentTime() { check(); return m_CurrentTimeInfo; }
	inline const CPUtility::VecDateInfo_t* GetCurrentDate() { check(); return &m_CurrentDateInfo; }
private:
	inline void timerUpdate() {
		update();
		m_UpdateDateTimer->expires_from_now(boost::posix_time::seconds(1));
		m_UpdateDateTimer->async_wait(boost::bind(&CPBoostLocalDate::timerUpdate, this));
	}

	inline void update() {
		CPUtility::GetStringDate(m_TodayDate, CPUtility::PUDToday);
		CPUtility::GetStringDate(m_YesterdayDate, CPUtility::PUDYesterday);
		CPUtility::GetStringTime(m_CurrentTimeInfo);
		CPUtility::GetCurrentDate(m_CurrentDateInfo);
	}

	inline void check() { if (nullptr == m_UpdateDateTimer) update(); }
private:
	BoostTimer_t*				m_UpdateDateTimer;
	std::string					m_TodayDate;
	std::string					m_YesterdayDate;
	std::string					m_CurrentTimeInfo;
	CPUtility::VecDateInfo_t	m_CurrentDateInfo;
};

#endif /* PBOOSTLOCALDATE_H_ */
