/*
 * PBoostLoadConfig.h
 *
 *  Created on: Oct 19, 2013
 *      Author: yuliang
 */

#ifndef PBOOSTLOADCONFIG_H_
#define PBOOSTLOADCONFIG_H_

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/typeof/typeof.hpp>

using namespace boost::property_tree;

class CPBoostLoadConfigBase {
public:
	CPBoostLoadConfigBase(){}
	virtual ~CPBoostLoadConfigBase() {}
	bool Init(const char* fname) { ptree pt; read_xml(fname, pt); return ParseFile(pt); }

protected:
	virtual bool ParseFile(const ptree& pt) = 0;
};


#endif /* PBOOSTLOADCONFIG_H_ */
