/*
 * PIOStreamOperator.cpp
 *
 *  Created on: Oct 22, 2013
 *      Author: yuliang
 */

#include "PIOStreamOperator.h"

std::ostream& operator<< (std::ostream& os, const std::vector<std::string>& vs) {
	for (std::vector<std::string>::const_iterator c_it = vs.begin(); c_it != vs.end(); ++c_it) {
		if (c_it != vs.begin())
			os << ",";
		os << *c_it;
	}
	return os;
}

std::ostream& operator<< (std::ostream& os, const std::vector<int32_t>& vs) {
	for (std::vector<int32_t>::const_iterator c_it = vs.begin(); c_it != vs.end(); ++c_it) {
		if (c_it != vs.begin())
			os << ",";
		os << *c_it;
	}
	return os;
}

std::ostream& operator<< (std::ostream& os, const std::list<std::string>& vs) {
	for (std::list<std::string>::const_iterator c_it = vs.begin(); c_it != vs.end(); ++c_it) {
		if (c_it != vs.begin())
			os << ",";
		os << *c_it;
	}
	return os;
}

std::vector<std::string>& operator<< (std::vector<std::string>& vs, const std::string& s) {
	vs.push_back(s);
	return vs;
}

std::vector<std::string>& operator<< (std::vector<std::string>& vs, const char* c) {
	vs.push_back(c);
	return vs;
}

std::vector<int32_t>& operator<< (std::vector<int32_t>& vi, int32_t i) {
	vi.push_back(i);
	return vi;
}

std::string& operator<< (std::string& ss, const char* ts) {
	ss.append(ts);
	return ss;
}

std::string& operator<< (std::string& ss, const std::string& ts) {
	ss.append(ts);
	return ss;
}

std::string& operator<< (std::string& ss, int8_t ti) {
	std::stringstream ost;
	ost << (int32_t)ti;
	ss.append(ost.str());
	return ss;
}

std::string& operator<< (std::string& ss, uint8_t ti) {
	std::stringstream ost;
	ost << (int32_t)ti;
	ss.append(ost.str());
	return ss;
}

std::string& operator<< (std::string& ss, int16_t ti) {
	std::stringstream ost;
	ost << (int32_t)ti;
	ss.append(ost.str());
	return ss;
}

std::string& operator<< (std::string& ss, uint16_t ti) {
	std::stringstream ost;
	ost << (int32_t)ti;
	ss.append(ost.str());
	return ss;
}

std::string& operator<< (std::string& ss, int32_t ti) {
	std::stringstream ost;
	ost << ti;
	ss.append(ost.str());
	return ss;
}

std::string& operator<< (std::string& ss, uint32_t ti) {
	std::stringstream ost;
	ost << ti;
	ss.append(ost.str());
	return ss;
}

std::string& operator<< (std::string& ss, int64_t ti) {
	std::stringstream ost;
	ost << ti;
	ss.append(ost.str());
	return ss;
}

std::string& operator<< (std::string& ss, uint64_t ti) {
	std::stringstream ost;
	ost << ti;
	ss.append(ost.str());
	return ss;
}

std::string& operator<< (std::string& ss, float tf) {
	char tMem[20] = {0};
	sprintf(tMem, "%.2f", tf);
	ss.append(tMem);
	return ss;
}

