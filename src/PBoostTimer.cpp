/*
 * PBoostTimer.cpp
 *
 *  Created on: Jun 21, 2014
 *      Author: yuliang
 */

#include "PBoostTimer.h"
#include "PException.h"
#include "PLog.h"

#define DEFAULT_TIMER_MIN_COUNT 100
#define DEFAULT_TIMER_MAX_COUNT 1000000

CPBoostTimer::CPBoostTimer() : m_TimerCount(0), m_Io(NULL) {
	// TODO Auto-generated constructor stub

}

CPBoostTimer::~CPBoostTimer() {
	// TODO Auto-generated destructor stub
}

void CPBoostTimer::Init(boost::asio::io_service* io) {
	m_Io = io;
	for (int32_t i = 0; i != DEFAULT_TIMER_MIN_COUNT; ++i) {
		boost::asio::deadline_timer* tTimer = newTimer();
		m_DeadTimer.push(tTimer);
	}
}

BoostTimer_t* CPBoostTimer::GetTimer() {
	if (NULL == m_Io) {
		throw USRERROR("null|boostio");
		return NULL;
	}

	if (m_DeadTimer.empty())
		return newTimer();

	BoostTimer_t* tTimer = m_DeadTimer.front();
	m_DeadTimer.pop();
	return tTimer;
}

void CPBoostTimer::SetTimer(BoostTimer_t* timer) {
	if (NULL == m_Io) {
		throw USRERROR("null|boostio");
		return;
	}

	if (NULL == timer)
		return;

	if (timer->expires_at() >= BoostTimer_t::traits_type::now())
		timer->cancel();
	m_DeadTimer.push(timer);
}

BoostTimer_t* CPBoostTimer::newTimer() {
	if (++m_TimerCount > DEFAULT_TIMER_MAX_COUNT) {
		LOG_ERROR("max|timer|" << m_TimerCount);
	}

	BoostTimer_t* tTimer = new boost::asio::deadline_timer(*m_Io);
	return tTimer;
}

