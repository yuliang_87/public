/*
 * PIOStreamOperator.h
 *
 *  Created on: Oct 22, 2013
 *      Author: yuliang
 */

#ifndef PIOSTREAMOPERATOR_H_
#define PIOSTREAMOPERATOR_H_

#include <vector>
#include <list>
#include <sstream>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

std::ostream& operator<< (std::ostream& os, const std::vector<std::string>& vs);
std::ostream& operator<< (std::ostream& os, const std::vector<int32_t>& vs);
std::ostream& operator<< (std::ostream& os, const std::list<std::string>& vs);
std::vector<std::string>& operator<< (std::vector<std::string>& vs, const std::string& s);
std::vector<std::string>& operator<< (std::vector<std::string>& vs, const char* c);
std::vector<int32_t>& operator<< (std::vector<int32_t>& vi, int32_t i);
std::string& operator<< (std::string& ss, const char* ts);
std::string& operator<< (std::string& ss, const std::string& ts);
std::string& operator<< (std::string& ss, int8_t ti);
std::string& operator<< (std::string& ss, uint8_t ti);
std::string& operator<< (std::string& ss, int16_t ti);
std::string& operator<< (std::string& ss, uint16_t ti);
std::string& operator<< (std::string& ss, int32_t ti);
std::string& operator<< (std::string& ss, uint32_t ti);
std::string& operator<< (std::string& ss, int64_t ti);
std::string& operator<< (std::string& ss, uint64_t ti);
std::string& operator<< (std::string& ss, float tf);

#endif /* PIOSTREAMOPERATOR_H_ */
