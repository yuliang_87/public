/*
 * PLog.h
 *
 *  Created on: Sep 6, 2013
 *      Author: yuliang
 */

#ifndef PLOG_H_
#define PLOG_H_

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <boost/thread.hpp>

#include <string>
#include <stdint.h>
using namespace log4cplus;

#define LOG_ROOT "root"

#define MLOG_TRACE(name, msg) {\
	Logger ____log = Logger::getInstance(name);\
	LOG4CPLUS_TRACE(____log, msg);\
	}
#define MLOG_DEBUG(name, msg) {\
	Logger ____log = Logger::getInstance(name); \
	LOG4CPLUS_DEBUG(____log, msg); \
	}
#define MLOG_INFO(name, msg) {\
	Logger ____log = Logger::getInstance(name);\
	LOG4CPLUS_INFO(____log, msg);\
	}
#define MLOG_WARN(name, msg) {\
	Logger ____log = Logger::getInstance(name);\
	LOG4CPLUS_WARN(____log, msg); \
	}
#define MLOG_ERROR(name, msg) {\
	Logger ____log = Logger::getInstance(name);\
	LOG4CPLUS_ERROR(____log, msg);\
	}

#define LOG_TRACE(msg) MLOG_TRACE(LOG_ROOT, msg)
#define LOG_DEBUG(msg) MLOG_DEBUG(LOG_ROOT, msg)
#define LOG_INFO(msg) MLOG_INFO(LOG_ROOT, msg)
#define LOG_WARN(msg) MLOG_WARN(LOG_ROOT, msg)
#define LOG_ERROR(msg) MLOG_ERROR(LOG_ROOT, msg)

/*
 * @brief       初始化配置信息
 * @auther      lyl
 * @param [in]  _fname : 配置文件名称
 * @param [in]  _sec   : 更新配置文件的时间间隔
 * @return      void
 * @note        根据_sec定时更新日志配置;若_sec为0,则不更新
 */
extern void Initlog(const std::string &_fname, uint32_t _sec);

#endif /* PLOG_H_ */
