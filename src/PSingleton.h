/*
 * PSingleton.h
 *
 *  Created on: May 13, 2014
 *      Author: yuliang
 */

#ifndef PSINGLETON_H_
#define PSINGLETON_H_
/*
 * CObject* obj = CSingleton<CObject>::GetInstance();
 *
 * CObject的构建函数和析构函数均为protected或private,则需要friend CSingleton
 */
template<typename T>
class CSingleton {
public:
	static T* GetInstance() {
		static T instance;
		return &instance;
	}
};


#endif /* PSINGLETON_H_ */
