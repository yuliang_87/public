/*
 * PStringOperator.h
 *
 *  Created on: Sep 25, 2013
 *      Author: yuliang
 */

#ifndef PSTRINGOPERATOR_H_
#define PSTRINGOPERATOR_H_

#include <string>
#include <vector>
#include <algorithm>

/*
 * @brief       字符串额外操作类
 * @auther      lyl
 */
class CPStringOperator {
public:
	typedef std::vector<std::string>	VecStr_t;
public:
	CPStringOperator();
	~CPStringOperator();

	/*
	 * @brief       去除左侧空格
	 * @auther      lyl
	 * @param [out] s      : 需操作的字符串
	 * @return      std::string : 原字符串的引用,用于连贯操作
	 */
	static inline std::string &LTrim(std::string &s) {
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return s;
	}

	/*
	 * @brief       去除右侧空格
	 * @auther      lyl
	 * @param [out] s      : 需操作的字符串
	 * @return      std::string : 原字符串的引用,用于连贯操作
	 */
	static inline std::string &RTrim(std::string &s) {
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}

	/*
	 * @brief       去除两侧空格
	 * @auther      lyl
	 * @param [out] s      : 需操作的字符串
	 * @return      std::string : 原字符串的引用,用于连贯操作
	 */
	static inline std::string &Trim(std::string &s) {
		return LTrim(RTrim(s));
	}

	/*
	 * @brief       分割字符串
	 * @auther      lyl
	 * @param [out] vs     : 分割后的字符串容器(例如vs:"","1","","2","","3","")
	 * @param [in]  s      : 原字符串(例如s:"_1_@2_@3_")
	 * @param [in]  c      : 分割表示字符串(例如c:"_@")
	 * @return      void
	 */
	static void SplitSome(VecStr_t& vs, const std::string &s, const std::string &c);
	static void SplitSome(VecStr_t& vs, const char *s, const char *c);

	/*
	 * @brief       分割字符串
	 * @auther      lyl
	 * @param [out] vs     : 分割后的字符串容器(例如vs:"_1","2","3_")
	 * @param [in]  s      : 原字符串(例如s:"_1_@2_@3_")
	 * @param [in]  c      : 分割表示字符串(例如c:"_@")
	 * @return      void
	 */
	static void SplitAll(VecStr_t& vs, const std::string &s, const std::string &c);
	static void SplitAll(VecStr_t& vs, const char *s, const char *c);
};

#endif /* PSTRINGOPERATOR_H_ */
