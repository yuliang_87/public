/*
 * OWSThread.h
 *
 *  Created on: Oct 17, 2013
 *      Author: yuliang
 */

#ifndef PTHREAD_H_
#define PTHREAD_H_

#include "PMailBoxs.h"

/*
 * @brief       线程执行体纯虚基类
 * @auther      lyl
 */
class CPThreadRunner {
public:
	CPThreadRunner() {}
	virtual ~CPThreadRunner() {}

	/*
	 * @brief       初始化
	 * @auther      lyl
	 * @return      void
	 */
	virtual void Init() { }

	/*
	 * @brief       线程执行函数
	 * @auther      lyl
	 * @param [in]  mail   : 消息
	 * @return      void
	 * @bote        使用完mail后,释放资源
	 */
	virtual void Run(mail_t mail) = 0;
};

/*
 * @brief       线程类
 * @auther      lyl
 * @param [in]  R      : 线程执行体
 */
template<typename R = CPThreadRunner>
class CPThread {
public:
	/*
	 * @brief       线程构造函数
	 * @auther      lyl
	 * @param [in]  infinite : 是否无限执行
	 * @param [in]  mailname : 接收消息的容器名称
	 * @param [in]  delself  : 线程执行完毕,是否释放自身内存
	 */
	CPThread(bool infinite = false, const char* mailname = NULL, bool delself = true) :
		m_delself(delself), m_infinite(infinite), m_mailname(mailname) {}
	virtual ~CPThread() {}

	/*
	 * @brief       线程启动函数
	 * @auther      lyl
	 * @return      void
	 */
	void Run();
private:
	bool m_delself; // 是否释放自身内存
	bool m_infinite; // 是否无限执行
	const char *m_mailname; // 接收消息的容器名称
};

template<typename R>
void CPThread<R>::Run() {
	// 创建线程执行体
	R *run_ = new R;
	// 初始化线程执行体
	run_->Init();

	for (;;) {
		mail_t mail = NULL;
		bool bRecv = true;

		if (NULL != m_mailname) {
			// 获取消息邮箱
			CPMailBoxs box(m_mailname);
			// 获取消息
			bRecv = box.Recv(mail);
		}

		if (bRecv)
			// 执行线程
			run_->Run(mail);

		if (!m_infinite)
			// 执行完毕,跳出线程
			break;
	}

	// 释放线程执行体
	delete run_;
	if (m_delself)
		// 释放线程
		delete this;
}

#endif /* PTHREAD_H_ */
