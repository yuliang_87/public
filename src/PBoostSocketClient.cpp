/*
 * PBoostSocketClient.cpp
 *
 *  Created on: May 20, 2014
 *      Author: yuliang
 */

#include "PBoostSocketClient.h"

#include <arpa/inet.h>
#include <string.h>

#include "PLog.h"

CTcpClient::CTcpClient(io_service& ioservice, uint32_t maxBufSize)
	: CClientCallbackBase(maxBufSize), m_Socket(ioservice),
	  m_Data(new char[maxBufSize]), m_SendFlag(0), m_UserData(NULL) {
	// TODO Auto-generated constructor stub
}

CTcpClient::~CTcpClient() {
	// TODO Auto-generated destructor stub
	closeSession();
	if (NULL != m_Data) delete[] m_Data;
}

void CTcpClient::SendData(const char *buf, uint32_t len) {
	if (!len || len > this->m_MaxBufSize - PKG_HEADER_LEN) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "param error, len|" << len);
		return;
	}
	if (!m_Socket.is_open()) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "socket not open.");
		return;
	}
	MLOG_DEBUG(LOG4CPLUS_TCP_SOCKET, "send data size|" << this << "|" << len);

	CMemoryPoolObject* tInstance = CSingleton<CMemoryPoolObject>::GetInstance();
	const char* tMemBuf = tInstance->GetMemoryPool()->PopMem(buf, len);
	if (NULL == tMemBuf) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "mem pool pop null.");
		return;
	}

	m_Socket.async_write_some(boost::asio::buffer(tMemBuf, len),
			boost::bind(&CTcpClient::handle_write, this, placeholders::error, _2, tMemBuf, len));
	++m_SendFlag;
}

void CTcpClient::SendLenAndData(const char *buf, uint32_t len) {
	if (!len || len > this->m_MaxBufSize - PKG_HEADER_LEN) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "param error, len|" << len);
		return;
	}
	if (!m_Socket.is_open()) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "socket not open.");
		return;
	}
	MLOG_DEBUG(LOG4CPLUS_TCP_SOCKET, "send data size|" << this << "|" << len);

	uint32_t totallen = len + PKG_HEADER_LEN;
	CMemoryPoolObject* tInstance = CSingleton<CMemoryPoolObject>::GetInstance();
	char* tMemBuf = tInstance->GetMemoryPool()->PopMem(len);
	if (NULL == tMemBuf) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "mem pool pop null.");
		return;
	}

	uint32_t nlen = htonl(len);
	memcpy(tMemBuf, &nlen, PKG_HEADER_LEN);
	memcpy(tMemBuf + PKG_HEADER_LEN, buf, len);

	m_Socket.async_write_some(boost::asio::buffer(tMemBuf, totallen),
			boost::bind(&CTcpClient::handle_write, this, placeholders::error, _2, tMemBuf, totallen));
	++m_SendFlag;
}

void CTcpClient::start() {
	if (!m_Socket.is_open()) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "socket has opened.");
		return;
	}
	if (m_MaxBufSize <=0 || (m_MaxBufSize & PKG_MAX_LEN)) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "buf is too long, max size|" << PKG_MAX_LEN << "|current size|" << m_MaxBufSize);
		return;
	}
	MLOG_TRACE(LOG4CPLUS_TCP_SOCKET, "one client connect|" << this << "|" << m_Socket.remote_endpoint().address().to_string());

	CMemoryPoolObject* tInstance = CSingleton<CMemoryPoolObject>::GetInstance();
	tInstance->Init(m_MaxBufSize);

	m_Socket.async_read_some(boost::asio::buffer(m_Data, m_MaxBufSize),
			boost::bind(&CTcpClient::handle_read, this, placeholders::error, placeholders::bytes_transferred));
}

void CTcpClient::handle_read(const error_code& err, std::size_t buflen) {
	if(!err) {
		MLOG_DEBUG(LOG4CPLUS_TCP_SOCKET, "reav data size|" << this << "|" << buflen);

		this->OnReceive(m_Data, buflen, this);
		m_Socket.async_read_some(boost::asio::buffer(m_Data, m_MaxBufSize),
				boost::bind(&CTcpClient::handle_read, this, placeholders::error, placeholders::bytes_transferred));
	} else {
		MLOG_TRACE(LOG4CPLUS_TCP_SOCKET, "disconnect|" << this << "|" << err.message());

		this->OnDisconnect(this);
	}
}
void CTcpClient::handle_write(const error_code& err, std::size_t slen, const char *buf, uint32_t len) {
	--m_SendFlag;
	if (slen != len) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "send error|" << this << "|" << len << "|" << slen);
	}
	if(err) {
		MLOG_TRACE(LOG4CPLUS_TCP_SOCKET, "disconnect|" << this << "|" << err.message());
		this->OnWriteError(buf, len, this);
	}

	CMemoryPoolObject* tInstance = CSingleton<CMemoryPoolObject>::GetInstance();
	tInstance->GetMemoryPool()->PushMem(const_cast<char *>(buf), len);
}

CTcpConnectClient::CTcpConnectClient(io_service& ioservice, uint32_t maxBufSize) : CTcpClient(ioservice, maxBufSize) {

}
CTcpConnectClient::~CTcpConnectClient() {

}

bool CTcpConnectClient::Connect(const std::string &ip, uint16_t port) {
	if (GetSocket().is_open()) {
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "socket has opened.");
		return false;
	}
	MLOG_TRACE(LOG4CPLUS_TCP_SOCKET, "ip|" << ip << "|port|" << port << "|connecting...");

	tcp::endpoint ep(address::from_string(ip), port);
	boost::system::error_code tErrno;
	GetSocket().connect(ep, tErrno);
	if (tErrno) {
		if (GetSocket().is_open())
			GetSocket().close();
		MLOG_ERROR(LOG4CPLUS_TCP_SOCKET, "socket connect fail, " << tErrno.message());
		return false;
	}
	start();
	return true;
}
