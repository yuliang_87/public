/*
 * SystemSignal.cpp
 *
 *  Created on: May 16, 2013
 *      Author: yuliang
 */

#include "SystemSignal.h"
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

CSystemSignal* CSystemSignal::instance = NULL;

CSystemSignal::CSystemSignal(bool bAnalysis) : m_bAnalysis(bAnalysis) {
	// TODO Auto-generated constructor stub
}

CSystemSignal::~CSystemSignal() {
	// TODO Auto-generated destructor stub
}

CSystemSignal* CSystemSignal::Getinstance(bool bAnalysis)
{
	if (NULL == instance) {
		instance = new CSystemSignal(bAnalysis);
	}
	return instance;
}

void CSystemSignal::AddUserSignal(UsrSignalBase *pUserSignal)
{
	if (NULL == pUserSignal || USER_SIG > pUserSignal->m_UsrSignal) {
		printf("UserSignal is err.\n");
		return;
	}
	this->m_mUserSig[pUserSignal->m_UsrSignal] = pUserSignal;
}

//
//void CSystemSignal::Init()
//{
//	PushSignal(CSignalFlag, sizeof(CSignalFlag) / sizeof(Signal_t));
//}
//
//void CSystemSignal::PushSignal(Signal_t iSig)
//{
//	this->m_sSignalNum.insert(iSig);
//}
//
//void CSystemSignal::PushSignal(const Signal_t *pSig, uint32_t uiSigLen)
//{
//	if (NULL == pSig && 0 >= uiSigLen)
//	{
//		return;
//	}
//	do
//	{
//		PushSignal(*pSig);
//		// 添加完最后一个
//		if (0 == --uiSigLen)
//			break;
//		pSig++;
//	} while (0 != uiSigLen);
//}

void CSystemSignal::StartCatch() const
{
	static bool bInit = false;
	if (!bInit) {
		for (uint32_t uiSig = 0; uiSig != sizeof(CSignalFlag) / sizeof(Signal_t); ++uiSig) {
			SetOneSignal(CSignalFlag[uiSig]);
		}
		bInit = true;
	}
}

void CSystemSignal::CatchSomeSignal(int iSigNum)
{
	switch (iSigNum) {
	case SIGINT:
		printf("收到Ctrl C信号.\n");
		exit(1);
		break;
	case SIGSEGV:
		printf("收到内存导致段错误信号.\n");
		CSystemSignal::Getinstance()->SegmentFault();
		exit(1);
		break;
	case SIGFPE:
		printf("收到数组越界导致段错误信号.\n");
		CSystemSignal::Getinstance()->SegmentFault();
		exit(1);
		break;
	case SIGPIPE:
		printf("收到管道破裂信号.\n");
		exit(1);
		break;
	default:
		{
			for (Map_UserSig::const_iterator c_it = CSystemSignal::Getinstance()->m_mUserSig.begin();
					c_it != CSystemSignal::Getinstance()->m_mUserSig.end(); ++c_it) {
				if (iSigNum == c_it->first) {
					c_it->second->CatchUsrSignal();
					break;
				}
			}
		}
		break;
	}
}

void CSystemSignal::SetOneSignal(Signal_t iSig) const
{
	if(signal(iSig, CSystemSignal::CatchSomeSignal) == SIG_ERR) {
		printf("Err: %s\n", strerror(errno));
		exit(1);
	}
}

void CSystemSignal::SegmentFault() const
{
	void *array[10];
	size_t size;
	char **strings;
	size_t i;

	size = backtrace (array, 10);
	strings = backtrace_symbols (array, size);

	char LogBuf[LOGBUFLEN];
	memset(LogBuf, 0, LOGBUFLEN);
	if (!this->GetLocalProcessName(LogBuf, LOGBUFLEN)) {
		printf("Get LocalProcessName Error.");
		return;
	}

	for (i = 0; i < size; i++) {
		if (!this->PrintSegvInfo(strings[i], LogBuf))
			break;
	}
	if (!this->m_bAnalysis) {
		memset(LogBuf, 0, LOGBUFLEN);
		sprintf(LogBuf, "echo \"可以使用:{addr2line -f [错误内存地址] -e [进程名称]}查看发送错误的代码. >> %s\"", SIGNAL_ERROR_LOG);
		system(LogBuf);
	}

	free (strings);
}

bool CSystemSignal::PrintSegvInfo(const char *pcSegvInfo, const char *pcProName) const
{
	if (NULL == pcSegvInfo || NULL == pcProName) {
		printf("Params Error.");
		return false;
	}
	std::string strCmd;
	if (this->m_bAnalysis) {
		std::string str(pcSegvInfo);

		strCmd.append("addr2line -f ");
		strCmd.append(str.substr(str.find("[", 0) + 1, str.find("]", 0) - str.find("[", 0) - 1));
		strCmd.append(" -e ");
		strCmd.append(pcProName);
		strCmd.append(" >> ");
		strCmd.append(SIGNAL_ERROR_LOG);
	} else {
		strCmd.append("echo \"");
		strCmd.append(pcSegvInfo);
		strCmd.append(" >> ");
		strCmd.append(SIGNAL_ERROR_LOG);
	}
	system(strCmd.c_str());
	return true;
}

bool CSystemSignal::GetLocalProcessName(char *pcBuf, int iBufLen) const
{
	char *pcProName = NULL;
	size_t iProNameLen = 0;
	ssize_t ssProNameLen = 0;

	char LogBuf[LOGBUFLEN];
	memset(LogBuf, 0, LOGBUFLEN);
	sprintf(LogBuf, "ps ax |grep \"%d\"| awk '{print $5}' > %s", getpid(), PRO_FILE_NAME);
	system(LogBuf);

	// 读文件
	FILE *fp = fopen(PRO_FILE_NAME, "r");
	ssProNameLen = getline(&pcProName, &iProNameLen, fp);
	fclose(fp);
	if (NULL == pcProName) {
		free(pcProName);
		return false;
	}
	// 拷贝数据
	if('.' == *pcProName)
		memcpy(pcBuf, pcProName + 2, ssProNameLen - 3);
	else
		memcpy(pcBuf, pcProName, ssProNameLen - 1);
	free(pcProName);

	memset(LogBuf, 0, LOGBUFLEN);
	sprintf(LogBuf, "rm -rf %s", PRO_FILE_NAME);
	system(LogBuf);
	return true;
}
