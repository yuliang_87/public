/*
 * PMailBoxs.h
 *
 *  Created on: Oct 18, 2013
 *      Author: yuliang
 */

#ifndef PMAILBOXS_H_
#define PMAILBOXS_H_

#include <map>
#include "PBoostMailBox.h"

typedef std::map<const char*, CBoostMailBox<void*>* > map_boxs_t;
typedef void* mail_t;

/*
 * @brief       消息邮箱集合
 * @auther      lyl
 * @note        int *i = new int; *i = 2; CPMailBoxs box("test"); box.Send(i);
 *              int *j = NULL; CPMailBoxs box("test"); box.Recv(j); delete j;// *j == 2
 */
class CPMailBoxs {
public:
	/*
	 * @brief       构造函数
	 * @auther      lyl
	 * @param [in]  boxname  : 消息邮箱名称
	 * @note        根据名称创建或查找邮箱
	 */
	CPMailBoxs(const char* boxname);
	virtual ~CPMailBoxs();

	/*
	 * @brief       发送消息邮件
	 * @auther      lyl
	 * @param [in]  mail     : 消息体
	 * @note        发送的mail必须是堆内存,接收后使用完毕释放
	 */
	inline void Send(const mail_t&mail) { m_box->Send(mail); }

	/*
	 * @brief       接收消息邮件
	 * @auther      lyl
	 * @param [out] mail   : 消息邮件
	 * @return      bool   : false表示未接收到消息邮件,true表示接收到消息邮件
	 * @note        接收到mail,使用完毕需要释放内存
	 */
	inline bool Recv(mail_t& mail) { return m_box->Recv(mail); }

	/*
	 * @brief       超时接收消息邮件
	 * @auther      lyl
	 * @param [out] mail   : 消息邮件
	 * @param [in]  msec   : 超时时间
	 * @return      bool   : false表示未接收到消息邮件,true表示接收到消息邮件
	 * @note        接收到mail,使用完毕需要释放内存
	 */
	inline bool TimeRecv(mail_t& mail, uint32_t msec) { return m_box->TimeRecv(mail, msec); }

	/*
	 * @brief       清空邮箱消息邮件
	 * @auther      lyl
	 * @return      void
	 */
	inline void Clear() { return m_box->Clear(); }

	/*
	 * @brief       移除某个消息邮箱
	 * @auther      lyl
	 * @param [in]  boxname  : 消息邮箱名称
	 * @note        必须确保已经没有别处使用此boxname的消息邮箱,否则会出现段错误.
	 */
	bool RemoveBox(const char *boxname);
private:
	CBoostMailBox<mail_t> *m_box;
	static boost::mutex m_mailboxsMutex;
	static map_boxs_t m_mailboxs;
};

class CPEvent {
public:
	CPEvent(const char *boxname, uint32_t e);
	virtual ~CPEvent();
	void Send(uint32_t e);
	bool Recv();
	bool TimeRecv();
private:
	uint32_t m_we;
	uint32_t m_re;
	CPMailBoxs *m_boxs;
};

#endif /* PMAILBOXS_H_ */
