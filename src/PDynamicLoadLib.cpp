/*
 * PDynamicLoadLib.cpp
 *
 *  Created on: Sep 9, 2013
 *      Author: yuliang
 */

#include "PDynamicLoadLib.h"

LHANDLE DLL_LoadLibrary(const char * file_name)
{
#ifdef _WIN32
	return LoadLibrary(file_name);
#else
	return dlopen(file_name, RTLD_NOW);
#endif
}

int DLL_FreeLibrary(LHANDLE lib)
{
#ifdef _WIN32
	return FreeLibrary(lib);
#else
	return dlclose(lib);
#endif
}

LPROCESS DLL_GetProcess(LHANDLE lib, const char * proc_name)
{
#ifdef _WIN32
	return GetProcAddress(lib, proc_name);
#else
	return dlsym(lib, proc_name);
#endif
}

const char * DLL_GetError()
{
#ifdef _WIN32
	return GetLastError();
#else
	return dlerror();
#endif
}

